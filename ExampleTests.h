#ifndef _EXAMPLE_TESTS
#define _EXAMPLE_TESTS

#include "cutest/TesterBase.hpp"

class ExampleTester : public cutest::TesterBase
{
public:
    ExampleTester()
        : TesterBase( "test_result_example.html" )
    {
        cutest::TesterBase::AddTest(cutest::TestListItem("Example1",  std::bind(&ExampleTester::Test_Example1, this)));
        cutest::TesterBase::AddTest(cutest::TestListItem("Example2",  std::bind(&ExampleTester::Test_Example2, this)));
    }

    virtual ~ExampleTester() { }

private:
    int Test_Example1();
    int Test_Example2();
};

int ExampleTester::Test_Example1()
{
  const std::string FUNCTION( __func__ );
  StartTestSet( "Test-" + FUNCTION, { /* Prerequisite functions */ } );

  /* ----------------------------------------------------------------------------- TEST */
  StartTest( "TEST 1: Description of this test" ); {
    Set_Comments( "Comments about this test, tips?" );

    int input1 = 2, input2 = 3;
    int expectedOutput1 = 5;
    int actualOutput1 = input1 + input2;
    int expectedOutput2 = -1;
    int actualOutput2 = input1 - input2;

    Set_Comments( "input1: " + ToString( input1 ) );
    Set_Comments( "input2: " + ToString( input2 ) );

    if      ( !Set_Outputs( "2+3", expectedOutput1, actualOutput1 ) ) { TestFail(); }
    else if ( !Set_Outputs( "2-3", expectedOutput2, actualOutput2 ) ) { TestFail(); }
    else                                                              { TestPass(); }
  } FinishTest();

  /* ----------------------------------------------------------------------------- TEST */
  StartTest( "TEST 2: Check that exception was thrown" ); {
    Set_Comments( "Expecting exception to be thrown" );
    bool expectedException = true;
    bool actualException = false;

    try
    {
      throw std::string( "example" );
    }
    catch( ... )
    {
      actualException = true;
    }

    if    ( !Set_Outputs( "exception thrown", expectedException, actualException ) ) { TestFail(); }
    else { TestPass(); }
  } FinishTest();

  FinishTestSet();
  return TestResult();
}

int ExampleTester::Test_Example2()
{
  const std::string FUNCTION( __func__ );
  StartTestSet( "Test-" + FUNCTION, { /* Prerequisite functions */ } );

  /* ----------------------------------------------------------------------------- TEST */
  StartTest( "TEST 3: Check that pointer is nullptr" ); {
    Set_Comments( "Comments about this test, tips?" );
    std::ostringstream oss;

    int * ptr = nullptr;
    oss << ptr;

    Set_ExpectedOutput( "ptr", "nullptr" );
    Set_ActualOutput( "ptr", ptr );

    if ( ptr != nullptr ) { TestFail(); }
    else { TestPass(); }
  } FinishTest();

  /* ----------------------------------------------------------------------------- TEST */
  // (This test fails, as an example)
  StartTest( "TEST 4: Check addition and subtraction results" ); {
    Set_Comments( "Example of a failing test" );

    int input1 = 2, input2 = 3;
    int expectedOutput1 = 5;
    int actualOutput1 = input1 * input2;
    int expectedOutput2 = -1;
    int actualOutput2 = input1 / input2;

    Set_Comments( "input1: " + ToString( input1 ) );
    Set_Comments( "input2: " + ToString( input2 ) );

    if      ( !Set_Outputs( "2+3", expectedOutput1, actualOutput1 ) ) { TestFail(); }
    else if ( !Set_Outputs( "2-3", expectedOutput2, actualOutput2 ) ) { TestFail(); }
    else                                                              { TestPass(); }
  } FinishTest();

  FinishTestSet();
  return TestResult();
}

#endif
